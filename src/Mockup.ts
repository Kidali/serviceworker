import ejs from 'ejs';

// @ts-ignore
import jsonConcat = require('json-concat');
import https = require('https');
import fss = require('fs-extra');
import fs from 'fs';

// Template
export class Mockup {
    public data: Infos[];
    public baseurl: null;
    public dir: null;
    public dirbk: null;
    public mergedestination: null;
    public endpoint: null;
    public table: null;
    constructor(infos: any, baseurl: any, dir: any, dirbk: any, mergedestination: any, endpont: any, table: any) {
        this.data = infos;
        this.baseurl = baseurl;
        this.dir = dir;
        this.dirbk = dirbk;
        this.mergedestination = mergedestination;
        this.endpoint = endpont;
        this.table = table;

        // Create Directory if doesnt exit and save data
        if (!fs.existsSync(this.dir) && !fs.existsSync(this.dirbk)) {
            fs.mkdirSync(this.dir);
            fs.mkdirSync(this.dir + '/' + this.dirbk);
            console.log('Done creating tmp dir..');
        }

        this.getData();
        // this.mergeFiles();
        // this.backupmergedfile();
    }

    // Caching data
    public getData() {
        https.get(`${this.baseurl}${this.endpoint}`, (resp) => {
            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                if (data.length > 0) {
                    const jsonData = '{"' + this.table + '":' + data + '}';
                    fs.writeFile(this.dir + '/' + this.table + '.json', jsonData, (err) => {
                        if (err) {
                            throw err;
                        }
                        console.log('Done writing: ' + this.table);
                    });
                }
            });

        }).on('error', (err) => {
            console.log('Error: ' + err.message);
        });

    }

    // Merging all cached tables
    public mergeFiles() {
        const tables: any = [];
        this.data.forEach((element: any) => {
            tables.push(this.dir + '/' + element.table + '.json');
        });

        jsonConcat({
            src: tables,
            dest: this.mergedestination,
        }, (json: any) => {
            console.log('merged');
        });
    }

    // Back up cached information
    public backupmergedfile() {
        fss.copySync(this.mergedestination, this.dir + '/' + this.dirbk + '/merged.json');
    }

    // Reset cached information
    public resetmergeddb() {
        fss.copySync(this.dir + '/' + this.dirbk + '/merged.json', this.mergedestination);
    }
}

// Interface declaration
interface Infos {
    endpoint: string;
    table: string;
}
