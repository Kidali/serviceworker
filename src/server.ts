import express from 'express';
import path from 'path';
import fs from 'fs';
import cron = require('node-cron');

import env from 'dotenv';
import { Mockup } from './Mockup';
env.config();

const app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(__dirname + '/'));

const entries = [
  { endpoint: 'todos', table: 'todos' },
  { endpoint: 'albums', table: 'albums' },
  { endpoint: 'posts', table: 'posts' },
  { endpoint: 'posts/1/comments', table: 'comments' },
  { endpoint: 'users', table: 'users' },
];

const baseurl = 'https://jsonplaceholder.typicode.com/';
const dir = 'mockserver';
const dirbk = 'mockserver_bk';
const mergedestination = 'mockserver/merged.json';

app.get('/', (req, res) => {
  res.render('index', { entries });
});

entries.forEach((entry: any) => {
  app.get('/' + entry.endpoint, (req, res) => {
    fs.readFile(process.env.BASE_DIR + '/' + entry.table + '.json', (err: any, data: any) => {
      if (err) { throw err; }
      const results = JSON.parse(data);
      res.send(results);
    });
  });
});

/*
   Cron job updates data after every 1 minute in case of
   a new response and pull
*/
cron.schedule('* * * * *', async () => {
  const infos = entries;

  let index;

  for (index = 0; index < infos.length; index++) {
    const api = infos[index];
    await makeMockupCall(infos, api);
  }

  // const api1 = infos[0];
  // await makeMockupCall(infos, api1);

  // const api2 = infos[1];
  // await makeMockupCall(infos, api2);

  // const api3 = infos[2];
  // await makeMockupCall(infos, api3);

  // const api4 = infos[3];
  // await makeMockupCall(infos, api4);

  // const api5 = infos[4];
  // await makeMockupCall(infos, api5);
});

function makeMockupCall(infos: any, api: any) {
  const m = new Mockup(infos, baseurl, dir, dirbk, mergedestination, api.endpoint, api.table);
}

app.listen(3000, () => console.log('app listening on port 3000!'));
