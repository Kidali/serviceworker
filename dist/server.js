"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const cron = require("node-cron");
const dotenv_1 = __importDefault(require("dotenv"));
const Mockup_1 = require("./Mockup");
dotenv_1.default.config();
const app = express_1.default();
app.set('view engine', 'ejs');
app.set('views', path_1.default.join(__dirname, 'views'));
app.use(express_1.default.static(__dirname + '/'));
const entries = [
    { endpoint: 'todos', table: 'todos' },
    { endpoint: 'albums', table: 'albums' },
    { endpoint: 'posts', table: 'posts' },
    { endpoint: 'posts/1/comments', table: 'comments' },
    { endpoint: 'users', table: 'users' },
];
const baseurl = 'https://jsonplaceholder.typicode.com/';
const dir = 'mockserver';
const dirbk = 'mockserver_bk';
const mergedestination = 'mockserver/merged.json';
app.get('/', (req, res) => {
    res.render('index', { entries });
});
entries.forEach((entry) => {
    app.get('/' + entry.endpoint, (req, res) => {
        fs_1.default.readFile(process.env.BASE_DIR + '/' + entry.table + '.json', (err, data) => {
            if (err) {
                throw err;
            }
            const results = JSON.parse(data);
            res.send(results);
        });
    });
});
/*
   Cron job updates data after every 1 minute in case of
   a new response and pull
*/
cron.schedule('* * * * *', () => __awaiter(this, void 0, void 0, function* () {
    const infos = entries;
    let index;
    for (index = 0; index < infos.length; index++) {
        const api = infos[index];
        yield makeMockupCall(infos, api);
    }
    // const api1 = infos[0];
    // await makeMockupCall(infos, api1);
    // const api2 = infos[1];
    // await makeMockupCall(infos, api2);
    // const api3 = infos[2];
    // await makeMockupCall(infos, api3);
    // const api4 = infos[3];
    // await makeMockupCall(infos, api4);
    // const api5 = infos[4];
    // await makeMockupCall(infos, api5);
}));
function makeMockupCall(infos, api) {
    const m = new Mockup_1.Mockup(infos, baseurl, dir, dirbk, mergedestination, api.endpoint, api.table);
}
app.listen(3000, () => console.log('app listening on port 3000!'));
//# sourceMappingURL=server.js.map