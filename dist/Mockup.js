"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// @ts-ignore
const jsonConcat = require("json-concat");
const https = require("https");
const fss = require("fs-extra");
const fs_1 = __importDefault(require("fs"));
// Template
class Mockup {
    constructor(infos, baseurl, dir, dirbk, mergedestination, endpont, table) {
        this.data = infos;
        this.baseurl = baseurl;
        this.dir = dir;
        this.dirbk = dirbk;
        this.mergedestination = mergedestination;
        this.endpoint = endpont;
        this.table = table;
        // Create Directory if doesnt exit and save data
        if (!fs_1.default.existsSync(this.dir) && !fs_1.default.existsSync(this.dirbk)) {
            fs_1.default.mkdirSync(this.dir);
            fs_1.default.mkdirSync(this.dir + '/' + this.dirbk);
            console.log('Done creating tmp dir..');
        }
        this.getData();
        // this.mergeFiles();
        // this.backupmergedfile();
    }
    // Caching data
    getData() {
        https.get(`${this.baseurl}${this.endpoint}`, (resp) => {
            let data = '';
            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });
            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                if (data.length > 0) {
                    const jsonData = '{"' + this.table + '":' + data + '}';
                    fs_1.default.writeFile(this.dir + '/' + this.table + '.json', jsonData, (err) => {
                        if (err) {
                            throw err;
                        }
                        console.log('Done writing: ' + this.table);
                    });
                }
            });
        }).on('error', (err) => {
            console.log('Error: ' + err.message);
        });
    }
    // Merging all cached tables
    mergeFiles() {
        const tables = [];
        this.data.forEach((element) => {
            tables.push(this.dir + '/' + element.table + '.json');
        });
        jsonConcat({
            src: tables,
            dest: this.mergedestination,
        }, (json) => {
            console.log('merged');
        });
    }
    // Back up cached information
    backupmergedfile() {
        fss.copySync(this.mergedestination, this.dir + '/' + this.dirbk + '/merged.json');
    }
    // Reset cached information
    resetmergeddb() {
        fss.copySync(this.dir + '/' + this.dirbk + '/merged.json', this.mergedestination);
    }
}
exports.Mockup = Mockup;
//# sourceMappingURL=Mockup.js.map